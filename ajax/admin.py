from django.contrib import admin
from .models import *

# Register your models here.
class TodoAdmin(admin.ModelAdmin):
    list_display = ['status','user','updated_date', 'created_date']
    list_filter = ['description']

admin.site.register(Todo, TodoAdmin)
