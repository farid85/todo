from django import forms
from .models import Todo
from django.forms import ModelForm, TextInput, Textarea, EmailInput, PasswordInput,URLField, IntegerField,ChoiceField, Select,NumberInput,URLInput, ImageField, FileField,FileInput, ChoiceField
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from collections import OrderedDict

class TodoForm(forms.ModelForm):
    # status = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control-status','id':'status'}), required=True)
    class Meta:
        model = Todo
        fields = ('description','status')
        widgets = {
            'description': Textarea(attrs={'class': 'form-control-todo','placeholder': 'Description *', 'required':'required','rows':'7','cols':'30','maxlength':'2000'}),
            'status':Select(attrs={'id': 'select-todo-action'})
            
        }
