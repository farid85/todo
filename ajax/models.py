from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.urls import reverse
from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.utils.text import slugify


# User=get_user_model()
# Create your models here.


class Todo(models.Model):
    STATUS = (
        ('True', 'Evet'),
        ('False','Hayır'),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.TextField(blank=True)
    status = models.CharField(max_length=10, choices=STATUS, default='False')
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    
    def __int__(self):
        return self.pk