from django.urls import path

from django.contrib.auth import views as auth_views
from . import views
from django.contrib.admin import helpers


urlpatterns = [
    path('', views.home, name="home"),
    path('todos', views.todos, name="todos"),
    path('todoCreate', views.todoCreate, name="todo-create"),
    path('todo/<id>/update/', views.todo_update, name="todo-update"),
    path('todo/<id>/delete/', views.todo_delete, name="todo-delete"),
]

handler404 = "ajax.views.error_404"
handler500 = "ajax.views.error_500"