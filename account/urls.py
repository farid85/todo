from django.urls import path

from django.contrib.auth import views as auth_views
from . import views


urlpatterns = [
    path('login', views.loginPage, name="login"),
    path('register', views.registerPage, name="register"),
    path('logout', views.logoutUser, name="logout"),
]